package se.hiq.oss.spring.nats.integration.app.nats;

import se.hiq.oss.json.schema.JsonSchema;

@JsonSchema(name = "owner",
            version = "1.0",
            location = "/integration/json-schema/owner.schema.json")
public class Owner {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
