package se.hiq.oss.spring.nats.autoconfigure;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import se.hiq.oss.json.schema.JsonSchemaDiscoverer;
import se.hiq.oss.json.schema.repo.JsonSchemaRepository;
import se.hiq.oss.spring.nats.config.NatsConfigProperties;
import se.hiq.oss.spring.nats.message.validation.JsonSchemaValidator;

@ConditionalOnProperty(prefix = "spring.nats", name = "jackson.json-schema.enable-validation", havingValue = "true", matchIfMissing = true)
@ConditionalOnClass({JsonSchemaRepository.class, ObjectMapper.class})
@Configuration
public class JsonSchemaBeans {
    @Autowired
    private NatsConfigProperties configProperties;

    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "spring.nats", name = "jackson.json-schema.scan-packages")
    @ConditionalOnBean(ObjectMapper.class)
    @Bean
    public JsonSchemaValidator jsonSchemaRepository(ObjectMapper objectMapper) {
        List<String> packages = configProperties.getJackson().getJsonSchema().getScanPackages();
        // this is an @NotEmpty property, so it should have at least one element
        String firstPackage = packages.remove(0);
        String[] additionalPackages = packages.stream().toArray(String[]::new);
        JsonSchemaRepository repository = new JsonSchemaDiscoverer(objectMapper, true)
                .discoverSchemas(firstPackage, additionalPackages);
        return new JsonSchemaValidator(repository);
    }
}
