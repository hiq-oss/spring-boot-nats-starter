package se.hiq.oss.spring.nats.autoconfigure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.micrometer.core.instrument.MeterRegistry;
import io.nats.client.Connection;
import se.hiq.oss.spring.nats.metrics.NatsMetricsRegistry;

@ConditionalOnClass(MeterRegistry.class)
@Configuration
public class MicrometerBeans {

    @Autowired
    private MeterRegistry meterRegistry;

    @Autowired
    private Connection natsConnection;

    @ConditionalOnBean(MeterRegistry.class)
    @Bean
    public NatsMetricsRegistry natsMetricsFactory() {
        return new NatsMetricsRegistry(meterRegistry, natsConnection);
    }

}
