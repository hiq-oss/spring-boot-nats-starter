package se.hiq.oss.spring.nats.autoconfigure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import se.hiq.oss.spring.nats.config.java.NatsProtobufConfiguration;

@ConditionalOnProperty(prefix = "spring.nats", name = "ser-de-type", havingValue = "PROTOBUF")
@Configuration
@Import(NatsProtobufConfiguration.class)
public class NatsProtobufBeans {
}
